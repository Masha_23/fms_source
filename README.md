# Feedback messages system 

## Installation

1. Install XAMPP or LAMP.

2. Start [apache] and [mysql] .

3. Download project from bitbucket(https://bitbucket.org/Masha_23/fms_source/src/master/). 
    
    OR follow gitbash commands 
    
    i>cd C:\\xampp\htdocs\ or i>cd /var/www/your_domain

    ii>git clone https://Masha_23@bitbucket.org/Masha_23/fms_source.git


4. extract files 

5. Create MySql db user [username: root] [password: '']

6. open any browser and type http://localhost/fms_source.

7. Here you will see a window where you can leave feedback and you can log in by selecting login or registration from the top right

