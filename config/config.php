<?php

class Config
{
    public static $tables = [
        'Users' => '`ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `username` varchar(128) NOT NULL, 
                    `password` varchar(128) NOT NULL,
                    `email` varchar(128) NOT NULL,
					`token` varchar(128) NOT NULL,
					PRIMARY KEY (ID)',
        'Messages' => '`ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `first_name` varchar(128) NOT NULL,
                    `last_name` varchar(128) NOT NULL,
                    `message` text NOT NULL,
                    `email` varchar(128) NOT NULL,
                    `date` DATETIME NOT NULL,
					PRIMARY KEY (ID)',
    ];
}


