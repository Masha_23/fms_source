<?php
require_once __DIR__.'/../model/message.php';

class Message
{
    public $model;
    private $firstName;
    private $lastName;
    private $email;
    private $message;
    private static $instance = null;

    public function __construct($data = array()) {
        $this->model = new messageModel();
        if (!empty($data)) {
            $this->firstName = $data['firstName'];
            $this->lastName = $data['lastName'];
            $this->email = $data['email'];
            $this->message = $data['message'];
        }
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }


    public function saveMessage() {
        $data = [
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'email' => $this->email,
            'message' => htmlspecialchars($this->message),
            'date' => date('Y-m-d H:i:s')
        ];

        $success = $this->model->insertMessage($data);

        print json_encode($success);
    }

    public function removeMessage($id) {
        $this->model->removeMessageById($id);
    }
}

if ($_POST['action'] === 'saveMessage') {
    $controller = new Message($_POST['body']['submittedData']);
    $controller->saveMessage();
}

if ($_POST['action'] === 'removeMessage') {
    $controller = Message::getInstance();
    $controller->removeMessage($_POST['body']['messageId']);
}

