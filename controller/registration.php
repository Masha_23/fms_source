<?php
require_once __DIR__.'/../model/registration.php';

class Registration
{
    public $model;
    private $username;
    private $password;
    private $email;

    public function __construct($data) {
        $this->model = new registerModel();
        $this->username = $data['username'];
        $this->email = $data['email'];
        $this->password = $data['password'];
    }

    public function signup() {
        $user = $this->model->checkUser($this->email);
        if ($user) {
            $result = array(
                'status' => 400,
                'message' => 'This User Already Exists'
            );
        }
        else {
            $data = array(
                'username' => $this->username,
                'password' => $this->password,
                'email' => $this->email
            );
            $result = $this->model->signUp($data);
        }

        print json_encode($result);
    }
}

if (isset($_POST['action']) && $_POST['action'] === 'registerUser') {
    $controller = new Registration($_POST['body']['submittedData']);
    $controller->signup();
}


