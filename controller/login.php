<?php
require_once __DIR__.'/../model/login.php';

class Login
{
    public $model;
    private $username;
    private $password;
    private static $instance;

    public function __construct($data = array()) {
        $this->model = new loginModel();
        if (!empty($data)) {
            $this->username = $data['username'];
            $this->password = $data['password'];
        }
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function login() {
        $user = $this->model->checkUser($this->username, $this->password);
        if ($user) {
            $result = $this->model->login($user['ID']);
        }
        else {
            $result = array(
                'status' => 400,
                'message' => 'Incorrect username or password'
            );
        }

        print json_encode($result);
    }

    public function logout() {
        $this->model->logout();
    }
}

if (isset($_POST['action']) && $_POST['action'] === 'loginUser') {
    $controller = new Login($_POST['body']['submittedData']);
    $controller->login();
}

if (isset($_GET['logout'])) {
    $controller = Login::getInstance();
    $controller->logout();
}

