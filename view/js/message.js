const getFieldsData = () => {
    let fields = jQuery('.fms-required-fields');
    let data = {};

    for (let field of fields) {
        let val = jQuery(field).val();
        if (!val.length) {
            jQuery(field).next().css( 'display', 'block' );

            return null;
        }
        data[jQuery(field).attr('name')] = val;
    }

    return data;
}

const saveMessage = (data) => {
    let ajaxData = {
        action: 'saveMessage',
        body: {
            submittedData: data,
            beforeSend: function() {
                jQuery('#fms-message-send').attr('disabled', true);
            }
        }
    };

    const temp = window.location.href.split('/');
    const ajaxurl = temp.slice(0, temp.length - 2).join('/')+'/controller/message.php';

    return jQuery.post(ajaxurl, ajaxData, function(response) {
        return response;
    });
}

const init = () => {
    jQuery('#fms-message-send').click(function(e){
        e.preventDefault();
        const data = getFieldsData();
        let html = ``;
        if (data) {
            const success = saveMessage(data);
            success.then(res => {
                res = JSON.parse(res);
                if (res.status === 200) {
                    html = `
                        <div class="alert alert-success">
                            <strong>Success!</strong> ${res.message}.
                        </div>  
                    `;
                }
                else {
                    html = `
                        <div class="alert alert-danger">
                            <strong>Danger!</strong> ${res.message}.
                        </div>                  
                    `;
                }
                jQuery('.form-content').html(html);
            });
        }
    });
}


jQuery(document).ready(function(){
    init();
});
