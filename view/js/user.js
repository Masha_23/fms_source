const getFieldsData = () => {
    let fields = jQuery('.fms-required-fields');
    let data = {};

    for (let field of fields) {
        let val = jQuery(field).val();
        if (!val.length) {
            jQuery(field).next().css( 'display', 'block' );

            return null;
        }
        data[jQuery(field).attr('name')] = val;
    }

    return data;
}

const registerUser = (data) => {
    let ajaxData = {
        action: 'registerUser',
        body: {
            submittedData: data,
            beforeSend: function() {
                jQuery('.fms-btn-reg').attr('disabled', true);
            }
        }
    };

    const temp = window.location.href.split('/');
    const ajaxurl = temp.slice(0, temp.length - 2).join('/')+'/controller/registration.php';

    return jQuery.post(ajaxurl, ajaxData, function(response) {
        return response;
    });
}

const loginUser = (data) => {
    let ajaxData = {
        action: 'loginUser',
        body: {
            submittedData: data,
            beforeSend: function() {
                jQuery('.fms-btn-reg').attr('disabled', true);
            }
        }
    };

    const temp = window.location.href.split('/');
    const ajaxurl = temp.slice(0, temp.length - 2).join('/')+'/controller/login.php';

    return jQuery.post(ajaxurl, ajaxData, function(response) {
        return response;
    });
}

const init = () => {
    jQuery('.fms-btn-reg').click(function(e){
        e.preventDefault();
        const action = jQuery('.fms-btn-reg').attr('data-attr-action');
        const data = getFieldsData();
        let html = ``;
        let result = {};

        if (data) {
            if (action == 'register') {
                result = registerUser(data);
            }
            else {
                result = loginUser(data);
            }
            result.then(res => {
                res = JSON.parse(res);
                if (res.status !== 200) {
                    html = `
                        <div class="alert alert-danger">
                            <strong>Danger!</strong> ${res.message}.
                        </div>
                    `;
                }
                else {
                    const temp = window.location.href.split('/');
                    const url = temp.slice(0, temp.length - 2).join('/')+'/view/home.php';
                    location.replace(url);
                }

                jQuery('.form-content').html(html);
            });
        }
    });
}

jQuery(document).ready(function(){
    init();
});
