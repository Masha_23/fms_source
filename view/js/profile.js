const filterMessages = () => {
    let filters = jQuery('.fms-filter-input');
    let data = {};
    for (let filter of filters) {
        let val = jQuery(filter).val();
        let name = jQuery(filter).attr('name');
        if (val) {
            data[name] = val;
        }
    }
    if (Object.keys(data).length > 0) {
        let ajaxData = {
            action: 'filterMessage',
            body: {
                filters: data,
                beforeSend: function() {
                    jQuery('.fms-btn-filter').attr('disabled', true);
                }
            }
        };

        const temp = window.location.href.split('/');
        const ajaxurl = temp.slice(0, temp.length - 2).join('/')+'/controller/message.php';

        return jQuery.post(ajaxurl, ajaxData, function(response) {
            return response;
        });
    }
}

const init = () => {
    jQuery('.fms-btn-view').click(function(e) {
        let id = jQuery(this).attr('data-attr-messageId');

        const temp = window.location.href.split('/');
        const url = temp.slice(0, temp.length - 2).join('/')+'/view/simpleMessage.php?id='+id;
        location.replace(url);
    });

    jQuery('.fms-btn-del').click(function(e){
        let id = jQuery(this).attr('data-attr-messageId');

        let ajaxData = {
            action: 'removeMessage',
            body: {
                messageId: id,
            }
        };

        const temp = window.location.href.split('/');
        const ajaxurl = temp.slice(0, temp.length - 2).join('/')+'/controller/message.php';

        return jQuery.post(ajaxurl, ajaxData, function(response) {
           location.reload();
        });
    });

    jQuery('.fms-btn-reset').click(function(){
        let temp = window.location.href;
        let url = temp.substr(0, temp.indexOf('?'));
        location.replace(url);
    });

    const urlParams = new URLSearchParams(window.location.search);
    jQuery('input[name="filterLastName"]').val(urlParams.get('filterLastName'));
    jQuery('input[name="filterName"]').val(urlParams.get('filterName'));
    jQuery('input[name="filterEmail"]').val(urlParams.get('filterEmail'));
}


jQuery(document).ready(function(){
    init();
});

