<?php
session_start();
if (!empty($_SESSION['token'])) {
    header('location: home.php');
}
?>

<div class="fms-main-wrapper">
    <div class="fms-header">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/user.css">
    </div>

    <div class="fms-container">
        <div class="fms-testbox">
            <form action="" method="post">
                <div class="banner">
                    <h1>Registration</h1>
                </div>
                <div class="form-content">
                    <div class="item">
                        <p>Username</p>
                        <input class="fms-required-fields" type="text" name="username" required/>
                        <span class="fms-required-message">This field is required</span>
                    </div>
                    <div class="item">
                        <p>Email</p>
                        <input class="fms-required-fields" type="email" name="email" required/>
                        <span class="fms-required-message">This field is required</span>
                    </div>
                    <div class="item">
                        <p>Password</p>
                        <input class="fms-required-fields" type="password" name="password" required/>
                        <span class="fms-required-message">This field is required</span>
                    </div>
                    <div class="btn-item">
                        <button class="fms-btn-reg" type="submit" href="" data-attr-action="register">SignUp</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="fms-footer">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/user.js"></script>
    </div>
</div>




