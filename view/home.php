<?php
session_start();
if (!empty($_SESSION['token'])) {
    header('location: profile.php');
}
?>

<div class="fms-main-wrapper">
    <div class="fms-header">
        <link rel="stylesheet" href="css/messageBox.css">
        <link rel="stylesheet" href="css/main.css">

        <a class="fms-link" href="login.php">Login</a>
        <a class="fms-link" href="registration.php">SignUp</a>
    </div>

    <!--  container start  -->
    <div class="fms-container">
        <div class="fms-testbox">
            <form action="" method="post">
                <div class="banner">
                    <h1>Write Feedback Message</h1>
                </div>

                <div class="form-content">
                    <div class="item">
                        <p>First Name</p>
                        <input class="fms-required-fields" type="text" name="firstName" required/>
                        <span class="fms-required-message">This field is required</span>
                    </div>
                    <div class="item">
                        <p>Last Name</p>
                        <input class="fms-required-fields" type="text" name="lastName" required/>
                        <span class="fms-required-message">This field is required</span>
                    </div>
                    <div class="item">
                        <p>Email</p>
                        <input class="fms-required-fields" type="email" name="email" required/>
                        <span class="fms-required-message">This field is required</span>
                    </div>
                    <div class="item">
                        <p>Additional notes</p>
                        <textarea class="fms-required-fields" rows="3" name="message" required></textarea>
                        <span class="fms-required-message">This field is required</span>
                    </div>
                    <div class="btn-item">
                        <button id="fms-message-send" type="submit" href="">Send</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--  container end  -->

    <div class="fms-footer">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/message.js"></script>
    </div>
</div>


