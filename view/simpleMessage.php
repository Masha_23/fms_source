<?php
require_once __DIR__.'/../model/message.php';
$obj = new messageModel();
$messageId = $_GET['id'];
$message = $obj->getMessageById($messageId);
?>

<div class="fms-main-wrapper">
    <div class="fms-header">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/messageCard.css">
        <a class="fms-link" href="../controller/login.php?logout=1">Logout</a>
    </div>

    <div class="fms-container">
        <div class="fms-message-card fms-message-card-page">
            <div class="fms-message-card-page-block1">
                <table>
                    <tr>
                        <th class="fms-table-title">Name</th>
                        <th class="fms-table-val"><?php echo $message['first_name']; ?></th>
                    </tr>
                    <tr>
                        <th class="fms-table-title">Last Name</th>
                        <th class="fms-table-val"><?php echo $message['last_name']; ?></th>
                    </tr>
                    <tr>
                        <th class="fms-table-title">Email</th>
                        <th class="fms-table-val"><?php echo $message['email']; ?></th>
                    </tr>
                    <tr>
                        <th class="fms-table-title">Date</th>
                        <th class="fms-table-val"><?php echo $message['date']; ?></th>
                    </tr>
                </table>
            </div>

            <div class="fms-message-card-page-block2">
                <p class="fms-table-title">Message</p>
                <p class="fms-message-page"><?php echo $message['message']; ?></p>
            </div>
        </div>
    </div>

    <div class="fms-footer">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/profile.js"></script>
    </div>
</div>
