<?php
require_once __DIR__.'/../model/message.php';
$obj = new messageModel();
$messages = $obj->getMessages($_GET);
session_start();
if (empty($_SESSION['token'])) {
    header('location: home.php');
}
?>

<div class="fms-main-wrapper">
    <div class="fms-header">
        <link rel="stylesheet" href="css/messageCard.css">
        <link rel="stylesheet" href="css/main.css">
        <a class="fms-link" href="../controller/login.php?logout=1">Logout</a>
    </div>

    <div class="fms-container">
        <h2>Feedback messages</h2>
        <div class="fms-filters">
            <form action="" method="get">
                <input class="fms-filter-input" type="text" placeholder="Filter By Name" name="filterName">
                <input class="fms-filter-input" type="text" placeholder="Filter By Email" name="filterEmail">
                <input class="fms-filter-input" type="text" placeholder="Filter By Last Name" name="filterLastName">

                <button type="submit" class="fms-btn fms-btn-filter">Filter</button>
                <button type="button" class="fms-btn fms-btn-reset">Reset</button>
            </form>
        </div>
        <?php foreach ($messages as $message) : ?>
            <div class="fms-message-card" style="width:50%;">
                <header class="fms-card-header">
                    <span class="fms-card-title"><?php echo $message['first_name'].' '.$message['last_name']; ?></span>
                    <span class="fms-card-date"><?php echo $message['date']; ?></span>
                    <span class="fms-card-email">email:<?php echo $message['email']; ?></span>
                </header>

                <div class="fms-card-container">
                    <p class="fms-message-page"><?php echo substr($message['message'], 0,600) ?>...</p>
                </div>

                <footer class="fms-card-footer">
                    <button class="fms-btn fms-btn-del" data-attr-messageId="<?php echo $message['ID']?>">Delete</button>
                    <button class="fms-btn fms-btn-view" data-attr-messageId="<?php echo $message['ID']?>">View More</button>
                </footer>
            </div>
        <?php endforeach ?>
    </div>
    <div class="fms-footer">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/profile.js"></script>
    </div>
</div>

