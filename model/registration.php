<?php
require_once __DIR__.'/../server/initDb.php';
session_start();

class registerModel
{
    public function checkUser($email) {
        $stmt = db::$tbdb->prepare('SELECT * FROM Users WHERE email =:email');
        $stmt->execute(['email' => $email]);
        $user = $stmt->fetch(\PDO::FETCH_ASSOC);

        return $user;
    }

    static function generateToken($email, $password) {
        $date = date("Y.m.d");
        $salt = md5('31052019');
        $secret = md5($email . $password . $date);
        $token = $salt . md5($salt . $secret);

        return $token;
    }

    public function signUp($data) {
        $result = array(
            'status' => 200,
            'message' => 'You are successfully logged in'
        );
        $data['token'] = self::generateToken($data['email'], $data['password']);
        $sql = 'INSERT INTO Users (username, password, email, token) VALUES (?, ?, ?, ?)';
        $stmt = db::$tbdb->prepare($sql);

        try {
            $success = $stmt->execute([$data['username'], $data['password'], $data['email'], $data['token']]);
            $_SESSION['token'] = $data['token'];
        }
        catch (Exception $e) {
            $result = array(
                'status' => 402,
                'message' => $e->getMessage()
            );
        }

        return $result;
    }
}
