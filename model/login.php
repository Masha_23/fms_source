<?php
require_once __DIR__.'/../server/initDb.php';
session_start();

class loginModel
{
    public function checkUser($username, $password) {
        $stmt = db::$tbdb->prepare('SELECT * FROM Users WHERE username = ? AND password = ?');
        $stmt->execute([$username, $password]);
        $user = $stmt->fetch(\PDO::FETCH_ASSOC);

        return $user;
    }

    static function generateToken($id) {
        $date = date("Y.m.d");
        $salt = md5('31052019');
        $secret = md5($id . $date);
        $token = $salt . md5($salt . $secret);

        return $token;
    }

    public function login($userId) {
        $token = self::generateToken($userId);
        $result = array(
            'status' => 200,
            'message' => 'You are successfully logged in'
        );
        $sql = 'UPDATE Users SET token = ? WHERE ID = ?';
        $stmt = db::$tbdb->prepare($sql);
        try {
            $success = $stmt->execute([$token, $userId]);

            if ($success) {
                $_SESSION['token'] = $token;
            }
        }
        catch (Exception $e) {
            $result = array(
                'status' => 402,
                'message' => $e->getMessage()
            );
        }

        return $result;
    }

    public function logout() {
        unset($_SESSION['token']);
        header('location: ../view/home.php');
    }
}
