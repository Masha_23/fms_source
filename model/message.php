<?php
require_once __DIR__.'/../server/initDb.php';

class messageModel
{
    public function insertMessage($data) {
        $result = array(
            'status' => 200,
            'message' => 'Your message successfully saved'
        );
        $sql = 'INSERT INTO Messages(first_name, last_name, message, email, date) VALUES (?, ?, ?, ?, ?)';

        $stmt = db::$tbdb->prepare($sql);
        try {
            $stmt->execute([$data['firstName'], $data['lastName'], $data['message'], $data['email'], $data['date']]);
        }
        catch (Exception $e) {
            $result = array(
                'status' => 402,
                'message' => $e->getMessage()
            );
        }

        return $result;
    }

    public function getMessages($data = array()) {
        $filters = array();
        foreach ($data as $key => $value) {
            if ($value) {
                $filters[$key] = $value;
            }
        }

        if (!empty($filters)) {
            $messages = $this->filterMessages($filters);

            return $messages;
        }

        $stmt = db::$tbdb->prepare('SELECT * FROM Messages');
        $stmt->execute();
        $messages = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $messages;
    }

    public function getMessageById($id) {
        $stmt = db::$tbdb->prepare('SELECT * FROM Messages WHERE ID = '.$id);
        $stmt->execute();
        $message = $stmt->fetch(\PDO::FETCH_ASSOC);

        return $message;
    }

    public function removeMessageById($id) {
        $stmt = db::$tbdb->prepare('DELETE FROM Messages WHERE ID = '.$id);
        $stmt->execute();
    }

    public function filterMessages($filters) {
        $sql = 'SELECT * FROM Messages WHERE';

        if (isset($filters['filterName'])) {
            $sql .= ' first_name LIKE '."'%".$filters['filterName']."%' AND";
        }
        if (isset($filters['filterLastName'])) {
            $sql .= ' last_name LIKE '."'%".$filters['filterLastName']."%' AND";
        }
        if (isset($filters['filterEmail'])) {
            $sql .= ' email LIKE '."'%".$filters['filterEmail']."%' AND";
        }

        $sql = substr($sql, 0, -3);
        $stmt = db::$tbdb->prepare($sql);
        $stmt->execute();
        $messages = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $messages;
    }
}
