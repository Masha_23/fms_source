<?php
require_once __DIR__.'/../config/config.php';

class db
{
    private $dbusername = 'root';
    private $dbpassword = '';
    private $host = 'mysql:host=localhost';
    private $dbname = 'Feedback_Messages_System';
    public static $tbdb;
    protected static $instance = null;

    public function __construct()
    {
        if (!isset(self::$tbdb)) {
            $this->initDbObject();
            $this->createDb();
            $this->createTables(Config::$tables);
        }
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function initDbObject() {
        self::$tbdb = new \PDO($this->host, $this->dbusername, $this->dbpassword);
        self::$tbdb->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function createDb() {
        self::$tbdb->query("CREATE DATABASE IF NOT EXISTS $this->dbname");
        self::$tbdb->query("use $this->dbname");
    }


    public function createTables($tablesData) {
        foreach ($tablesData as $tableName => $tableQuery) {
            $sql = 'CREATE TABLE IF NOT EXISTS '.$tableName .'('. $tableQuery .') ENGINE=InnoDB DEFAULT CHARSET=utf8;';

            self::$tbdb->query($sql);
        }
    }
}

new db();








