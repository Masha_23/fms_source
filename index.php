<?php

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);


if (!defined('FMS_PROJET_FILE_NAME')) {
    define('FMS_PROJET_FILE_NAME', dirname(__FILE__).'/');
}

require_once FMS_PROJET_FILE_NAME.'server/initDb.php';
header('location: view/profile.php');

